# docker Drupal
# VERSION       0.3
FROM    savoirfairelinux/lampd
MAINTAINER Ernesto Rodriguez Ortiz <ernesto.rodriguezortiz@savoirfairelinuc.com>

ENV initpath ./fabfile/docker

COPY ${initpath} /opt/init
RUN /opt/init/bootstrap

ENTRYPOINT ["/opt/init/init"]
CMD ["/sbin/my_init"]
