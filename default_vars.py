from os import path

from fabric.api import env


# Project settings

env.project_name = 'drupalizer-default'
env.workspace = path.join(path.dirname(__file__), path.pardir)


# Site

env.site_root = '{}/src/drupal'.format(env.workspace)
env.site_name = 'Default Drupalizer'
env.site_environment = 'local'
env.site_profile = 'standard'
env.site_profile_repo = ''
env.site_profile_makefile = 'drupalizer.make'
env.site_profile_branch= '7.x'
env.site_db_user = 'dev'
env.site_db_pass = 'dev'
env.site_db_host = 'localhost'
env.site_db_name = 'dev'
env.site_hostname = 'drupalizer.dev.local'
env.site_admin_user = 'admin'
env.site_admin_pass = 'admin'
env.site_subdir = 'default'
env.site_languages = ''
env.site_default_language = ''

# PatternLab

# Specify the PatternLab dir is you want the style guide to be generated

env.patternlab_dir = ''


# Database dump
# To enable it, replace the boolean value with the absolute path of a gzipped SQL dump file.

env.db_dump = False


# Docker

env.docker_workspace = '/opt/sfl'
env.docker_site_root = '{}/src/drupal'.format(env.docker_workspace)
env.bind_port = 8001
env.apache_user = 'www-data'

# Docker auto-added container IP

env.container_ip = '172.17.0.2'


# Hook commands
# Example:
# env.hook_post_install = ['drush fra -y', 'drush cc all']
# env.hook_post_update = ['drush fra -y', 'drush cc all']

env.hook_post_install = []
env.hook_post_update = []


# Target environments definition

env.aliases = {}
