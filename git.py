import re
import time

from os import path

from fabric.api import task, env, execute
from fabric.api import task, env, local
from fabric.colors import red, green, yellow
from fabric.contrib.console import confirm

import helpers as h

from .environments import e


@task(alias='is_dirty')
def check_status():
    """
    Check workspace's git repositories status.
    """
    if (is_git_dirty()):
        print red('Your workspace is not clean.')
    else:
        print green('Your workspace is clean.')


def is_git_dirty():
    repos = local('find ' + path.normpath(env.workspace) + ' -name ".git"', capture=True).splitlines()
    repos = _remove_untouched_repository(repos)

    nbWarnings = 0
    for repo in repos:
        repoLocalPath = path.normpath(path.join(repo, '..'))
        nbWarnings += _check_repo(repoLocalPath)

    return (nbWarnings > 0)


    # STEP 2
    # plutot que s'arreter ou continuer betement, on est intelligent et on demande quoi faire:
    # - s'il y a du code non-stage, en faire un commit
    # - si la Branch n'est pas trackee, la pusher
    # - s'il y a des commits non pushes, les pusher
    # - attention aux conflits, faire un pull d'abord et valider la fusion automatique


def _remove_untouched_repository(repos):
    repos.remove(path.normpath(path.join(env.workspace, '.git')))
    repos.remove(path.normpath(path.join(env.workspace, 'fabfile', '.git')))

    return repos


def _check_repo(repoLocalPath):
    nbWarnings = 0
    with h.fab_cd('local', repoLocalPath):
        print green('---')
        print green('Verify repo in ' + repoLocalPath)

        remoteName = local('LC_ALL=C git remote', capture=True)

        filesStatusRawInfo = _get_files_status_information()
        print green('Verify local files status against current HEAD commit...')
        nbWarnings += _check_files_status_vs_head_commit(filesStatusRawInfo)

        localBranchesRawInfo = _get_local_branches_information()
        print green('Verify local branches exist on remote "' + remoteName + '"...');
        nbWarnings += _check_local_branches_exist_on_remote(localBranchesRawInfo, remoteName)

        print green('Verify branches status against remote...');
        nbWarnings += _check_local_branches_status_vs_remote(localBranchesRawInfo, remoteName)

        return nbWarnings


def _check_files_status_vs_head_commit(filesStatusRawInfo):
    nbWarnings = 0
    addableFiles = []
    if (len(filesStatusRawInfo) > 0):
        for fileStatus in filesStatusRawInfo:
            fileStatusData = fileStatus.split()
            # Break loop if filename is "fabfile"
            if fileStatusData[1] == 'fabfile':
                break

            nbWarnings += 1
            addableFiles.append(fileStatusData[1])

            print yellow('File "' + fileStatusData[1] + '" ' + {
                'M': 'has un-commited modifications.',
                'D': 'has been deleted.',
                '??': 'is not indexed.',
            }.get(fileStatusData[0], 'is in an unknown state (' + fileStatusData[0] + ')'))

    return nbWarnings


def _check_local_branches_exist_on_remote(localBranchesRawInfo, remoteName):
    nbWarnings = 0
    pushableBranches = []
    for localBranchRawInfo in localBranchesRawInfo:
        localBranchName = _get_branch_name(localBranchRawInfo)
        if ((localBranchName is not None) and (not _remote_branch_exists(localBranchName))):
            nbWarnings += 1
            pushableBranches.append(localBranchName)
            print yellow('Local branch "' + localBranchName + '" is not present on "' + remoteName + '" remote.')

    # On suggere de pusher la(les) branche(s) qui sont seulement sur le local
    if (nbWarnings > 0):
        if (confirm(red('There are many local branches not present on remote. Do you want to sync theses?'), default=False)):
            for branchName in pushableBranches:
                local('git push --set-upstream ' + remoteName + ' ' + branchName)

            # Do not alert with diff as local branches are now pushed
            nbWarnings = 0

    return nbWarnings


def _check_local_branches_status_vs_remote(localBranchesRawInfo, remoteName):
    nbWarnings = 0
    pushableBranches = []
    pattern = re.compile('.*\[.* (ahead) .*\].*')
    for localBranchRawInfo in localBranchesRawInfo:
        if (pattern.match(localBranchRawInfo)):
            localBranchName = _get_branch_name(localBranchRawInfo)
            nbWarnings += 1
            pushableBranches.append(localBranchName)
            print yellow('Local branch "' + localBranchName + '" is ahead of remote branch.');

    # On suggere de pusher la(les) branche(s) qui sont seulement sur le local
    if (nbWarnings > 0):
        if (confirm(red('There are many local branches which are ahead of remote branch. Do you want to sync theses?'), default=False)):
            for branchName in pushableBranches:
                local('git push ' + remoteName + ' ' + branchName)

            # Do not alert with diff as local branches are now pushed
            nbWarnings = 0

    # TODO
    # PUSHER LA BRANCHE SUR LE REPO

    return nbWarnings


def _get_local_branches_information():
    return local('git branch --list -vv', capture=True).splitlines()


def _get_branch_name(branchRawData):
    branchName = branchRawData.replace('*', '').strip()
    if (_is_branch_detached(branchName)):
        return None
    else:
        return branchName.split()[0]


def _is_branch_detached(branchName):
    pattern = re.compile('\(.*\)')
    return pattern.match(branchName)


def _remote_branch_exists(branchName):
    return (len(local('git branch --list --remote "*' + branchName + '"', capture=True).splitlines()) > 0)


def _get_files_status_information():
    return local('git status -s', capture=True).splitlines()
