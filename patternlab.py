from __future__ import unicode_literals

from fabric.api import task, roles, env, local, lcd
from fabric.colors import green

import helpers as h


@task
@roles('local')
def build():
    """
    Generate the PatternLab static site
    """
    workspace = env.docker_workspace
    host = env.site_hostname
    site_root = env.docker_site_root

    with lcd(env.patternlab_dir):
        local('touch public/styleguide/html/styleguide.html')
        local('php core/builder.php -g')
